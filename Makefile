#basic complex latex makefile
.SILENT: 
all: ch1 ch2 bibliography book clean

ch1: ch1.tex
	latex ch1.tex
	latex ch1.tex
	dvips ch1.dvi
	ps2pdf ch1.ps	

ch2: ch2.tex
	latex ch2.tex
	latex ch2.tex
	dvips ch2.dvi
	ps2pdf ch2.ps
	
#add for more chapter that could compile by itself.
bibliography: 
	latex book.tex
	bibtex book
book: book.tex bibliography
	latex book.tex
	latex book.tex
	dvips book.dvi
	ps2pdf book.ps
clean:
	rm -f *.aux *.dvi *.ps *.log *.nav *.out *.ps *.snm *.toc
	rm -f *.blg *.bbl
#have another make if you want to combine the two .tex's into one big file for instance ch1.tex and ch2.tex could lead to
#book: ch1.aux ch2.aux
#   latex book.tex
#where book.tex will have input ch1 and input ch2
